import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.isEmpty()) {
            return null;
        }
        int min = liste.get(0);
        for (Integer i : liste) {
            if (i < min) {
                min = i;
            }
        }
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T item : liste) {
            if (item.compareTo(valeur) < 1) {
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2) {

        ArrayList<T> lR = new ArrayList<>();

        if (liste1.equals(liste2) && !liste1.isEmpty()) {
            T prec = liste1.get(0);
            lR.add(prec);
            for (T item :
                    liste1) {
                if (item != prec) {
                    lR.add(item);
                }
                prec = item;

            }
            return lR;
        }

        int pos_l1 = 0;
        int pos_l2 = 0;

        while (pos_l1 <= liste1.size() - 1 && pos_l2 <= liste2.size() - 1) {
            switch (liste1.get(pos_l1).compareTo(liste2.get(pos_l2))) {

                case 0:
                    if (!lR.contains(liste1.get(pos_l1))) {
                        lR.add(liste1.get(pos_l1));
                    }
                    pos_l1++;
                    pos_l2++;

                case 1:
                    pos_l2++;

                case -1:
                    pos_l1++;

            }
        }

        System.out.println(lR);
        return lR;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        String word = "";
        ArrayList<String> listStrings = new ArrayList<>();
        for (char c : texte.toCharArray()) {
            if (c == ' ' && !word.equals("")) {
                listStrings.add(word);
                word = "";
            } else if (c != ' ') {
                word += c;
            }
        }
        if (!word.equals("")) {
            listStrings.add(word);
        }
        return listStrings;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if (texte.equals("")) {
            return null;
        }

        HashMap<String, Integer> mots = new HashMap<>();
        ArrayList<String> texteList = (ArrayList<String>) decoupe(texte);

        for (String mot : texteList) {
            if (mots.containsKey(mot)) {
                mots.put(mot, mots.get(mot) + 1);
            } else {
                mots.put(mot, 1);
            }
        }
        String maxVal = "";
        mots.put("", 0);
        for (String mot : mots.keySet()) {
            if (mots.get(mot).equals(mots.get(maxVal))) {
                ArrayList<String> s = new ArrayList<>();
                s.add(mot);
                s.add(maxVal);
                System.out.println(mot + " / " + maxVal);
                maxVal = Collections.min(s);
                System.out.println("= " + maxVal);
            }
            if (mots.get(mot) > mots.get(maxVal)) {
                maxVal = mot;
            }
        }
        return maxVal;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if (chaine.isEmpty())
            return true;

        int nbPar = 0;
        System.out.println(chaine);

        for (char c : chaine.toCharArray()) {
            if (c != '(') {
                nbPar -= 1;
            }
            if (c != ')') {
                nbPar += 1;
            }
            System.out.println(nbPar);
            if (nbPar < 0) {
                return false;
            }

        }

        return nbPar == 0;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        int CrochRight = 0;
        int CrochLeft = 0;
        int ParLeft = 0;
        int ParRight = 0;
        boolean returned = true;
        int i = 0;

        if (chaine.isEmpty()) {
            return true;
        } else if (chaine.length() == 1) {
            return false;
        } else {
            while (i < chaine.length() && returned) {
                switch (chaine.charAt(i)) {
                    case '(': {
                        ParLeft += 1;
                        if (i == chaine.length() - 1 || (i < chaine.length() - 1 && chaine.charAt(i + 1) == ']')) {
                            returned = false;
                        }
                        break;
                    }

                    case ')': {
                        ParRight += 1;
                        if (i == 0 || (i > chaine.length() - 1 && chaine.charAt(i - 1) == '[')) {
                            returned = false;
                        }
                        break;
                    }
                    case '[': {
                        CrochLeft += 1;
                        if (i == chaine.length() - 1 || (i < chaine.length() - 1 && chaine.charAt(i + 1) == ')')) {
                            returned = false;
                        }
                        break;
                    }
                    case ']': {
                        CrochRight += 1;
                        if (i == 0 || (i > chaine.length() - 1 && chaine.charAt(i - 1) == '(')) {
                            returned = false;
                        }
                        break;
                    }
                }

                i += 1;
            }
            if (ParLeft != ParRight || CrochLeft != CrochRight) {
                returned = false;
            }
        }
        return returned;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.size() > 0) {
            int center;
            int max = liste.size();
            int min = 0;

            while (min < max) {
                center = ((min + max) / 2);
                if (valeur.compareTo(liste.get(center)) == 0) {
                    return true;
                } else if (valeur.compareTo(liste.get(center)) > 0) {
                    min = center + 1;
                } else {
                    max = center;
                }
            }
        }
        return false;

    }

}
